# Why error handling is important with promises and how it has to be done

## What is promises?

The Promise object represents the eventual completion (or failure) of an asynchronous operation and its resulting value.

## Error Handling with Promises

It is very important to handle the error while writing code because it's very difficult to detect the error in case of ascynchronus function and it can be done using the promise chaining and `catch()`.

Promise chains is a very good option for error handling. When a promise is rejected, the control jumps to the closest rejection handler. 

A Promise will be in one of these 3 states:

* pending : initial state, neither fulfilled nor rejected.
* fulfilled : means the operation was completed successfully.
* rejected : means the operation is failed.

The eventual state of a pending promise should be fulfilled with a value or rejected with an error. When either of these options occurs, the promise’s `then` method is called. If the promise has already been fulfilled then the code inside `then` will be executed or if it gets rejected then the code inside `catch` will be executed

## Creation of new Promise:

```
let promise = new Promise(function(resolve, reject) {
    let error = false;   // lets assume there is no error
  if(error){
    reject();
  } else {
    resolve();      // if there is no error then only this else block is executed
  }
});
```

arguments `resolve` and `reject` are callbacks provided by the JavaScript promise itself. Our code is only inside the executor.

* `resolve(value)` — if the execution is finished successfully, with result value.
* `reject(error)` — if an error has occurred, an error is the error object.

Code Example:

```
const pr = new Promise((resolve, reject) => {
  resolve("Successful output");
});

pr.then((value) => {
  console.log(value);  // Successful output
  throw new Error("some error accured!");
})
.catch((err) => {
    console.error(err);   // this will console error
  })
```

using promise with fs 
```
let pr = new Promise((resolve, reject) => {
        fs.readFile(someRandomfilePath, "utf-8", (err, data) => {     
            if (err) {
                console.error("Error in the reading the file");
                reject(err);
            } else {
                console.log("started reading the file ....");
                resolve(data);
            }
        });
    })
    .then(()=>{
        doSomething();   // calling another function only if the above promise is resolved
    })
    .catch((err)=>{
        console.log(err);
    })

```